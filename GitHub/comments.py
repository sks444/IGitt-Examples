from gh_token import GH_TOKEN
from IGitt.GitHub import GitHubToken
from IGitt.GitHub.GitHubComment import GitHubComment

comment = GitHubComment(GitHubToken(GH_TOKEN), 'coala/community', 'issues', 373075029)

#Get the comment object
print (comment)#Getting NotImplementedError

#Get the id of the comment
print (comment.number)

#Get the type of the comment
print (comment.type)

#Get the descritption of the comment
print (comment.body)

#Get the author of the comment
print (comment.author)

#Get the time when this comment was created
print (comment.created)

#Get the time when this comment was updated
print (comment.updated)

#Get the repository where this comment belongs
print (comment.repository)
