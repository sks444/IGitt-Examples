from gh_token import GH_TOKEN
from IGitt.GitHub import GitHubToken
from IGitt.GitHub.GitHubRepository import GitHubRepository

repo = GitHubRepository(GitHubToken(GH_TOKEN), 'coala/community')

#Get a single repo
print (repo)

#Get the id of the repo
print (repo.identifier)

#Get the org of the repo
print (repo.top_level_org)

#Get the name of the repo
print (repo.full_name)

#Get set of commits to the repo
commits = repo.commits

#Get list of commits to the repo
commits_list = []
for commit in commits:
	commits_list.append(commit)

#Get a single commit
commit = commits_list[0]
#For more methods on a commit
#See commit.py

#Get clone url of the repo
print (repo.clone_url)

#Get the labels of the repo
labels = repo.get_labels()
print (labels)

#Get set of issues to the repo
print (repo.issues)

#Get a single issue by its number
print (repo.get_issues(1))
#For getting more details of a
#issue see issues.py

#Get set of MRs to the repo
print (repo.merge_requests)

#Get a single MR of the repo by its number
print (repo.get_mr(77))
#For getting more details of a
#MR see pull_requests.py

#Get all the urls this repo is hooked to
print (repo.hooks)
