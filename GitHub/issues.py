from gh_token import GH_TOKEN
from IGitt.GitHub import GitHubToken
from IGitt.GitHub.GitHubIssue import GitHubIssue

issue = GitHubIssue(GitHubToken(GH_TOKEN), 'coala/community', 1)

#Get the repo where this issue belong
print (issue.repository)
#For more details on a repo
#see repository.py

#Get the title of the issue
print (issue.title)

#Get the number of the issue
print (issue.number)

#Get the assignees to the issue
print (issue.assignees)

#Get the description of the issue
print (issue.description)

#Get the author of the issue
print (issue.author)

#Get the comments to a issue
comments = issue.comments
print (comments)
#For getting more details on a comment
#see comments.py

#Get the labels on a issue
print (issue.labels)

#Get the available labels
print (issue.available_labels)

#Get the time when issue was created
print (issue.created)

#Get the time when issue was last updated
print (issue.updated)

#Get the state of the issue
print (issue.state)

#Get reactions to the issue
print (issue.reactions)

#Get the PR which close this issue
print (issue.mrs_closed_by)
#For getting more details on PR
#see pull_requests.py
