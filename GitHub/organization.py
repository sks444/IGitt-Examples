from gh_token import GH_TOKEN
from IGitt.GitHub import GitHubToken
from IGitt.GitHub.GitHubOrganization import GitHubOrganization

org = GitHubOrganization(GitHubToken(GH_TOKEN), 'coala')

#Get the web_url of the org
print (org.web_url)

#Get the description of the org
print (org.description)

#Get the number of registered users for the org
print (org.billable_users)

#Get all the owners to the org
owners = org.owners

#Get a single owner
owners_list = []
for owner in owners:
	owners_list.append(owner)
owner = owners_list[0]

#Get the id of the single owner
print (owner.identifier)

#Get the username of the owner
print (owner.username)

#Get install_repo of the owner
print (owner.installed_repositories)

#Get the masters of the org
print (org.masters)

#Get the name of the org
print (org.name)

#Get the suborgs of the org
print (org.suborgs)

#Get the set of repositories for the org
repos = org.repositories
print (repos)

#Get a list of repositories for the org
repos_list = []
for repo in repos:
	repos_list.append(repo)

#Get a single repo
repo = repos_list[0]
#For getting details of a single repo
#Look in repository.py
