from gh_token import GH_TOKEN
from IGitt.GitHub import GitHubToken
from IGitt.GitHub.GitHubCommit import GitHubCommit

commit = GitHubCommit(GitHubToken(GH_TOKEN), 'coala/community', 'b951ad95948112785522428d66a78785ffb7eebc')

#Get the commit object
print (commit)

#Get the commit message
print (commit.message)

#Get sha of the commit
print (commit.sha)

#Get the repository of the commit
print (commit.repository)
#for more details of a repository
#please see repository.py

# Get the parent commit. In case of a merge 
#commit the first parent will be returned.
print (commit.parent)

#Get all the commit statuses
print (commit.statuses)

#Get combined status of all commits
print (commit.combined_status)

#Get the patch for a given file
print (commit.get_patch_for_file('README.md'))

#Get the unified diff for the commit
#excluding the diff index
print (commit.unified_diff)
