from gh_token import GH_TOKEN
from IGitt.GitHub import GitHubToken
from IGitt.GitHub.GitHubUser import GitHubUser

user = GitHubUser(GitHubToken(GH_TOKEN), 'sks444')

#Get the username
print (user.username)

#Get the id of the user
print (user.identifier)

#Get installed repositories of the user
print (user.installed_repositories)
