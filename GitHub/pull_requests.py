from gh_token import GH_TOKEN
from IGitt.GitHub import GitHubToken
from IGitt.GitHub.GitHubMergeRequest import GitHubMergeRequest

pr = GitHubMergeRequest(GitHubToken(GH_TOKEN), 'coala/community', 52)

#Get the PR object
print (pr)

#Get the base commit as a commit object
print (pr.base)

#Get the sha of the base
print (pr.base.sha)

#Get the head commit as a commit object
print (pr.head)

#Get the sha of the head
print (pr.head.sha)

#Get the base branch name
print (pr.base_branch_name)

#Get the head branch name
print (pr.head_branch_name)

#Get the tuple of commit objects
#that are included in the PR.
print (pr.commits)

#Get the repository where this comes from.
print (pr.repository)

#Get the repository where this 
#PR's head branch is located at
print (pr.source_repository)
#For getting more details of repo
#see repository.py

#Get the affected files by this PR
print (pr.affected_files)

#Get the addition and deletion of the PR
print (pr.diffstat)

#Get set of issue objects which
#would be closing upon merging this PR
print (pr.closes_issues)

#Get set of issue obejects which are 
#to this pull request
print (pr.mentioned_issues)

#Get the author of the PR
print (pr.author)

#Get the state of the PR
print (pr.state)

#Get the comments on a PR
comments = pr.comments
print (pr.comments)
#For getting more details of
#a comment see comments.py
